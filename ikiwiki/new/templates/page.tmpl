<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="<TMPL_VAR BASEURL>style.css" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="#3b80ae" name="theme-color">
    <meta property="og:site_name" content="freedesktop.org">
    <meta property="og:type" content="website">
    <meta property="og:image" content="/icon.png">
    <meta property="twitter:image" content="/icon.png">
    <link rel="icon" href="/icon.png" sizes="16x16" type="image/png">
    <link rel="alternate" type="application/x-wiki" title="Edit this page" href="<TMPL_VAR EDITURL>">
    <title><TMPL_VAR TITLE> · freedesktop.org</title>
    <meta property="og:title" content="<TMPL_VAR TITLE> · freedesktop.org">
    <meta property="twitter:title" content="<TMPL_VAR TITLE> · freedesktop.org">

    <TMPL_IF FEEDLINKS>
      <TMPL_VAR FEEDLINKS>
    </TMPL_IF>
    <TMPL_IF RELVCS>
      <TMPL_VAR RELVCS>
    </TMPL_IF>
    <TMPL_IF META>
      <TMPL_VAR META>
    </TMPL_IF>
    <TMPL_LOOP TRAILLOOP>
      <TMPL_IF PREVPAGE>
        <link rel="prev" href="<TMPL_VAR PREVURL>" title="<TMPL_VAR PREVTITLE>" />
      </TMPL_IF>
        <link rel="up" href="<TMPL_VAR TRAILURL>" title="<TMPL_VAR TRAILTITLE>" />
      <TMPL_IF NEXTPAGE>
        <link rel="next" href="<TMPL_VAR NEXTURL>" title="<TMPL_VAR NEXTTITLE>" />
      </TMPL_IF>
    </TMPL_LOOP>
  </head>
  <body>
    <!-- works around FireFox bug https://bugzilla.mozilla.org/show_bug.cgi?id=1404468 -->
    <script>0</script>
    <nav>
      <a href="#main" class="main-content" tabindex="0">Skip to Main Content</a>
      <div class="nav-grid">
        <div>
          <a aria-label="Homepage" href="http://freedesktop.org">
            <img alt="" class="logo">
          </a>
        </div>

        <div class="nav-column-align-center nav-column-align-right">
          <TMPL_IF SEARCHFORM>
            <TMPL_VAR SEARCHFORM>
          </TMPL_IF>
        </div>

        <div class="nav-column-align-center">
          <TMPL_LOOP PARENTLINKS>
            <a class="nav-link-button dim-label" href="<TMPL_VAR URL>"><TMPL_VAR PAGE></a> /
          </TMPL_LOOP>
          <b><TMPL_VAR TITLE></b>
          <TMPL_IF ISTRANSLATION>
            &nbsp;(<TMPL_VAR PERCENTTRANSLATED>%)
          </TMPL_IF>
        </div>

        <div class="nav-column-align-right">
          <a class="nav-link-button" href="<TMPL_VAR EDITURL>">Edit</a>
          <TMPL_IF RECENTCHANGESURL>
            <a class="nav-link-button" href="<TMPL_VAR RECENTCHANGESURL>">RecentChanges</a>
          </TMPL_IF>

          <TMPL_IF HISTORYURL>
            <a class="nav-link-button" href="<TMPL_VAR HISTORYURL>">Page History</a>
          </TMPL_IF>

          <TMPL_IF GETSOURCEURL>
            <a class="nav-link-button" href="<TMPL_VAR GETSOURCEURL>">Source</a>
          </TMPL_IF>
        </div>
      </div>
    </nav>

    <main id="main">
      <TMPL_VAR CONTENT>
      <TMPL_UNLESS DYNAMIC>
      <TMPL_IF COMMENTS>
        <section id="comments">
        <TMPL_VAR COMMENTS>
        <TMPL_IF ADDCOMMENTURL>
          <div class="addcomment">
          <a href="<TMPL_VAR ADDCOMMENTURL>">Add a comment</a>
          </div>
        <TMPL_ELSE>
          <div class="addcomment">Comments on this page are closed.</div>
        </TMPL_IF>
        </section>
      </TMPL_IF>
      </TMPL_UNLESS>
    </main>

    <footer>
      <div class="footer-text">
      <TMPL_UNLESS DYNAMIC>
        <TMPL_VAR TRAILS>

        <TMPL_IF TAGS>
          <p>
            Tags:
            <TMPL_LOOP TAGS>
              <TMPL_VAR LINK>
            </TMPL_LOOP>
          </p>
        </TMPL_IF>

        <TMPL_IF BACKLINKS>
          <p class="backlinks-row">
            Links:
            <TMPL_LOOP BACKLINKS>
              <a class="nav-link-button" href="<TMPL_VAR URL>"><TMPL_VAR PAGE></a>
            </TMPL_LOOP>
            <TMPL_IF MORE_BACKLINKS>
              <TMPL_LOOP MORE_BACKLINKS>
                <a class="nav-link-button" href="<TMPL_VAR URL>"><TMPL_VAR PAGE></a>
              </TMPL_LOOP>
            </TMPL_IF>
          </p>
        </TMPL_IF>

        <TMPL_IF COPYRIGHT>
          <p>
            <a name="pagecopyright"></a>
            <TMPL_VAR COPYRIGHT>
          </p>
        </TMPL_IF>

        <TMPL_IF LICENSE>
          <p>
            <a name="pagelicense"></a>
            License: <TMPL_VAR LICENSE>
          </p>
        </TMPL_IF>

        <p><i>Last edited <TMPL_VAR MTIME></i></p>

        <TMPL_IF EXTRAFOOTER><TMPL_VAR EXTRAFOOTER></TMPL_IF>
        </TMPL_UNLESS>
      </div>
    </footer>
  </body>
</html>
